﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class SQLServer
    {
        public static string ConnectString = string.Empty;

        public static int Update(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectString);

            //创建SQLCommend
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);

            int count = 0;
            try
            {
                sqlConnection.Open();
                count = sqlCommand.ExecuteNonQuery();

                MessageBox.Show("SQL语句执行成功！");
                return count;

            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show("插入失败" + ex.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        /// <summary>
        /// 查询方法
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectString);

            //创建SQLCommend
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                sqlConnection.Open();
                sqlDataAdapter.Fill(dataSet);
                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show("插入失败" + ex.Message);
            }
            finally
            {
                sqlConnection.Close();
            }

        }
    }
}
