﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using MySql.Data.MySqlClient;
using static WindowsFormsApp1.Home;

namespace WindowsFormsApp1
{
    public partial class 登录界面 : Form
    {

        public bool isStart = false;
        public Home mainHome;

        public 登录界面()
        {
            InitializeComponent();
        }

        private void 登录界面_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Color FColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(251)))));
            Color TColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(234)))));

            Brush b = new LinearGradientBrush(this.ClientRectangle, TColor, FColor, LinearGradientMode.ForwardDiagonal);
            g.FillRectangle(b, this.ClientRectangle);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Home home = new Home();


            String Username, Password;
            Username = usertext.Text;
            Password = passtext.Text;

            if (passtext.Text == "sun" && usertext.Text == "user")
            {
                mainHome.isLogin = true;
                this.Close();//登录成功后关闭登录窗体
            }
            else if(usertext.Text == "" && passtext.Text == "")
            {
                MessageBox.Show("请输入用户名和密码");
            }
            else if(usertext.Text == "user" && passtext.Text == "")
            {
                MessageBox.Show("请输入密码");

            }else if(usertext.Text == "" && passtext.Text != "sun")
            {
                MessageBox.Show("请输入用户名");
            }
            else
            {
                MessageBox.Show("用户名,密码错误");
            }


            #region 
            /* String connStr = "server = localhost;user id= root;password = 20001020;database = my_data;charset = utf8";

             MySqlConnection DBconn = new MySqlConnection();
             DBconn.ConnectionString = connStr;

             try
             {
                 DBconn.Open();
                 DialogResult result = MessageBox.Show("连接数据库成功","连接状态",MessageBoxButtons.OK);
                 //MessageBox.Show("连接成功");
                 string sql = "select *from 两学期综合排名";
                 MySqlDataAdapter mda = new MySqlDataAdapter(sql, DBconn);
                 DataSet ds = new DataSet();
                 mda.Fill(ds, "table");

                 //DialogResult result = MessageBox.Show("连接成功");



                 //this.dataGridView1.DataSource = ds.Tables["table"];
             }
             catch (Exception ex)
             {

                 MessageBox.Show("服务器未开启，通过命令“net start mysql_sun开启”" + System.Environment.NewLine + ex.Message);
             }*/
            #endregion
        }

        private void button3_Click(object sender, EventArgs e)
        {
            登录界面 frm = new 登录界面();

            this.Close();


        }
        
        private void 登录界面_Load(object sender, EventArgs e)
        {            

            String connStr = "server = localhost;user id= root;password = 20001020;database = my_data;charset = utf8";

            MySqlConnection DBconn = new MySqlConnection();
            DBconn.ConnectionString = connStr;

            try
            {
                DBconn.Open();
                label3.Text = "数据库已连接";
                label3.BackColor = Color.GreenYellow;
                //DialogResult result = MessageBox.Show("连接数据库成功", "连接状态", MessageBoxButtons.OK);
                //MessageBox.Show("连接成功");
                string sql = "select *from 两学期综合排名";
                MySqlDataAdapter mda = new MySqlDataAdapter(sql, DBconn);
                DataSet ds = new DataSet();
                mda.Fill(ds, "table");
            }
            catch
            {
                label3.Text = "数据库未连接";
                label3.BackColor = Color.Red;
                //MessageBox.Show("服务器未开启，通过命令“net start mysql_sun开启”" + System.Environment.NewLine + ex.Message);
            }

             test frm = new test();
            frm.Logmain = this;
            frm.ShowDialog();

            /* if (isStart)
            {
                //MessageBox.Show("成功");可以不显示
            }
            else
            {
                //this.Close();
            }
            */

        }


        
    }
}
