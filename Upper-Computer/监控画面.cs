﻿using S7.Net;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;



namespace WindowsFormsApp1
{
    public partial class 监控画面 : Form
    {

        Plc St30;
        bool m100;
        bool m101;
        bool m102;
        bool m103;
        bool m104;
        bool m105;
        bool m106;
        bool m107;
        bool Q00;
        bool Inputtag;//确认写入标志
        bool Readtag;//确认读取标志

        public 监控画面()
        {
            InitializeComponent();
        }

        /// <summary>
        /// PLC通讯连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            System.Console.Beep(3000, 200);
            string ip = Convert.ToString(textBox1.Text);
            //string ip2 = St30.Read);

            if (label2.Text != "连接成功")
            {
                try
                {
                    St30 = new Plc(CpuType.S7200Smart, ip, 0, 1);
                    St30.Open();
                    label2.Text = "连接成功";
                    label2.BackColor = Color.Green;
                    button1.Text = "断开";
                    timer1.Start();
                    Thread.Sleep(1000);
                }
                catch (NullReferenceException ex2)
                {
                    MessageBox.Show("IP地址为空，连接超时\n" + ex2.Message);
                }
                catch (Exception ex1)
                {
                    Thread.Sleep(0);
                    label2.Text = "连接失败";
                    label2.BackColor = Color.Red;
                    MessageBox.Show("连接超时\n" + ex1.Message);
                }
            }
            else
            {
                St30.Close();
                label2.Text = "连接断开";
                label2.BackColor = Color.Red;
                button1.Text = "连接";
                timer1.Stop();

            }
        }

        /// <summary>
        /// 数据读取
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            m100 = (bool)St30.Read("M10.0");
            m101 = (bool)St30.Read("M10.1");
            m102 = (bool)St30.Read("M10.2");
            m103 = (bool)St30.Read("M10.3");
            m104 = (bool)St30.Read("M10.4");
            m105 = (bool)St30.Read("M10.5");
            m106 = (bool)St30.Read("M10.6");
            m107 = (bool)St30.Read("M10.7");
            Q00 = (bool)St30.Read("Q0.0");


            int x = Convert.ToInt32(numericUpDown2.Value);

            //按键确认写入
            if (Inputtag)
            {

                if (comboBox1.Text == "V" && comboBox2.Text == "D")
                {
                    numericUpDown1.Maximum = 4294967295;
                    numericUpDown1.Minimum = -2147483648;


                    Int32 a3 = Convert.ToInt32(numericUpDown1.Value);

                    float a4_1 = Convert.ToSingle(numericUpDown1.Value);

                    if (comboBox4.Text == "有符号" || comboBox4.Text == "无符号")
                    {
                        St30.Write(DataType.DataBlock, 1, x, a3);
                    }
                    else if (comboBox4.Text == "浮点数")
                    {
                        St30.Write(DataType.DataBlock, 1, x, a4_1);
                    }

                    //写入VD区

                    //当前值显示 VD10
                    //textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.DWord, 1));
                    //textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, (VarType)Convert.ToDouble(VarType.DWord), 1));
                    //当前值显示 VD
                    //小数显示 3位小数

                    if (comboBox4.Text == "浮点数")
                    {
                        textBox2.Text = Convert.ToString(Math.Round((Convert.ToDouble(St30.Read(DataType.DataBlock, 1, x, VarType.Real, 1))), 3));
                    }
                    else if (comboBox4.Text == "有符号")
                    {
                        /* if (numericUpDown1.Value >=0)
                        {
                            textBox2.Text = "+" + Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.DWord, 1));
                        }
                        else 
                        {
                            textBox2.Text = "-" + Convert.ToString(St30.Read(DataType.DataBlock, 1, x, (VarType)Convert.ToInt32(VarType.DWord), 1));
                        }*/
                        textBox2.Text = "-" + Convert.ToString(St30.Read(DataType.DataBlock, 1, x, (VarType)Convert.ToInt32(VarType.DWord), 1));

                    }
                    else
                    {
                        textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.DWord, 1));

                    }

                }
                else if (comboBox1.Text == "V" && comboBox2.Text == "W")
                {
                    numericUpDown1.Maximum = 65535;
                    numericUpDown1.Minimum = -32768;

                    Int16 a3 = Convert.ToInt16(numericUpDown1.Value);
                    //写入VW区
                    St30.Write(DataType.DataBlock, 1, x, (Int16)a3);
                    //当前值显示
                    textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.Word, 1));
                    //St30.Write(comboBox1.Text+comboBox2.Text+comboBox3.Text, (Int16)a3);
                }
                else if (comboBox1.Text == "V" && comboBox2.Text == "B")
                {
                    numericUpDown1.Maximum = 255;
                    numericUpDown1.Minimum = -127;

                    byte a3 = Convert.ToByte(numericUpDown1.Value);
                    byte[] x1 = { a3 };
                    //写入VB区
                    St30.WriteBytes(DataType.DataBlock, 1, Convert.ToUInt16(numericUpDown2.Value), x1);
                    //当前值显示
                    textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.Byte, 1));
                }
                else if (comboBox1.Text == "M" && comboBox2.Text == "D")
                {
                    numericUpDown1.Maximum = 4294967295;
                    numericUpDown1.Minimum = -2147483648;

                    Int32 a4 = Convert.ToInt32(numericUpDown1.Value);
                    St30.Write(comboBox1.Text + comboBox2.Text + numericUpDown2.Value, a4);
                    //当前值显示 VD10
                    textBox2.Text = Convert.ToString(St30.Read(comboBox1.Text + comboBox2.Text + numericUpDown2.Value));
                }
                else if (comboBox1.Text == "M" && comboBox2.Text == "W")
                {
                    numericUpDown1.Maximum = 65535;
                    numericUpDown1.Minimum = -32768;

                    Int16 a5 = Convert.ToInt16(numericUpDown1.Value);
                    //写入VW区
                    St30.Write(comboBox1.Text + comboBox2.Text + numericUpDown2.Value, a5);
                    //当前值显示
                    textBox2.Text = Convert.ToString(St30.Read(comboBox1.Text + comboBox2.Text + numericUpDown2.Value));
                }
                //写入按钮标志
                Inputtag = false;
            }

            if (Readtag)
            {
                if (comboBox1.Text == "V" && comboBox2.Text == "D")
                {
                    if (comboBox4.Text == "浮点数")
                    {
                        //小数显示 3位小数
                        textBox2.Text = Convert.ToString(Math.Round((Convert.ToDouble(St30.Read(DataType.DataBlock, 1, x, VarType.Real, 1))), 3));
                        //textBox2.Text = Convert.ToString( Convert.ToDouble(St30.Read(DataType.DataBlock, 1, x, VarType.Real, 1)));
                    }
                    else if (comboBox4.Text == "二进制")
                    {
                        textBox2.Text = Convert.ToString(Math.Round((Convert.ToDouble(St30.Read(DataType.DataBlock, 1, x, VarType.Byte, 1))), 3));
                    }
                    else
                    {
                        textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.DWord, 1));
                    }


                }
                else if (comboBox1.Text == "V" && comboBox2.Text == "W")
                {
                    textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.Word, 1));
                }
                else if (comboBox1.Text == "V" && comboBox2.Text == "W")
                {
                    textBox2.Text = Convert.ToString(St30.Read(DataType.DataBlock, 1, x, VarType.Byte, 1));
                }
                else if (comboBox1.Text == "M" && comboBox2.Text == "D")
                {

                }
                //读取按钮标志
                Readtag = false;
            }

            #region M位状态 读取显示位 右侧显示
            /////////////////////////////////////////
            if (m100)
            {
                label9.Text = "true";
            }
            else
            {
                label9.Text = "flase";
            }
            /////////////////////////////////////////
            if (m101)
            {
                label10.Text = "true";
            }
            else
            {
                label10.Text = "flase";
            }
            //////////////////////////////////////////
            if (m102)
            {
                label11.Text = "true";
            }
            else
            {
                label11.Text = "flase";
            }
            //////////////////////////////////////////
            if (m103)
            {
                label12.Text = "true";
            }
            else
            {
                label12.Text = "flase";
            }
            //////////////////////////////////////////
            if (m104)
            {
                label14.Text = "true";
            }
            else
            {
                label14.Text = "flase";
            }
            //////////////////////////////////////////
            if (m105)
            {
                label16.Text = "true";
            }
            else
            {
                label16.Text = "flase";
            }
            //////////////////////////////////////////
            if (m106)
            {
                label18.Text = "true";
            }
            else
            {
                label18.Text = "flase";
            }
            //////////////////////////////////////////
            if (m107)
            {
                label20.Text = "true";
            }
            else
            {
                label20.Text = "flase";
            }
            //////////////////////////////////////////
            if (Q00)
            {
                label13.Text = "电机旋转";
            }
            else
            {
                label13.Text = "电机停止";
            }
            #endregion 
        }
        /// <summary>
        /// M10.0 M10.1操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);
            if (label2.Text == "连接成功")
            {
                if (m100)
                {
                    St30.Write("M10.0", 0);
                    button2.ForeColor = Color.Red;
                    Thread.Sleep(100);
                    St30.Write("M10.0", 1);
                    button2.ForeColor = Color.Green;
                }
                else
                {
                    St30.Write("M10.0", 1);
                    button2.ForeColor = Color.Green;
                    Thread.Sleep(100);
                    St30.Write("M10.0", 0);
                    button2.ForeColor = Color.Red;
                }
            }
        }
        /// <summary>
        /// M10.0 M10.1操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button3_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);
            if (label2.Text == "连接成功")
            {
                if (m101)
                {
                    St30.Write("M10.1", 0);
                    button3.ForeColor = Color.Red;
                    Thread.Sleep(100);
                    St30.Write("M10.1", 1);
                    button3.ForeColor = Color.Green;
                }
                else
                {
                    St30.Write("M10.1", 1);
                    button3.ForeColor = Color.Green;
                    Thread.Sleep(100);
                    St30.Write("M10.1", 0);
                    button3.ForeColor = Color.Red;
                }
            }
        }
        /// <summary>
        /// 确认写入按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click_1(object sender, EventArgs e)
        {
            System.Console.Beep(3000, 200);
            Inputtag = true;
        }

        /// <summary>
        /// 确认读取按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            System.Console.Beep(3000, 200);
            Readtag = true;
        }

        private void panel1_SizeChanged(object sender, EventArgs e)
        {
            panel1.Refresh();
        }


        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load_1(object sender, EventArgs e)
        {
            timer2.Start();
            this.label24.Show();

            comboBox1.Items.Add("M");
            comboBox1.Items.Add("V");
            comboBox1.Items.Add("Q");

            comboBox2.Items.Add("B");
            comboBox2.Items.Add("W");
            comboBox2.Items.Add("D");

            comboBox4.Items.Add("有符号");
            comboBox4.Items.Add("无符号");
            comboBox4.Items.Add("浮点数");
            comboBox4.Items.Add("二进制");
            comboBox4.Text = "有符号";
        }
        /// <summary>
        /// 系统时间显示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            label25.Text = "系统时间:" + System.DateTime.Now.ToString();
        }

        //界面边框颜色设置////
        #region
        /// <summary>
        /// 绿色边框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                this.panel1.ClientRectangle,
                Color.Orange,
                3,
                ButtonBorderStyle.Solid,
                Color.Orange,
                3,
                ButtonBorderStyle.Solid,
                Color.Orange,
                3,
                ButtonBorderStyle.Solid,
                Color.Orange,
                3,
                ButtonBorderStyle.Solid
                );
        }

        /// <summary>
        /// 数据读取框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                this.panel3.ClientRectangle,
                Color.LightSeaGreen,
                0,
                ButtonBorderStyle.Solid,
                Color.LightSeaGreen,
                3,
                ButtonBorderStyle.Solid,
                Color.LightSeaGreen,
                0,
                ButtonBorderStyle.Solid,
                Color.LightSeaGreen,
                3,
                ButtonBorderStyle.Solid
                );
        }

        /// <summary>
        /// PLC连接框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel2_Paint_1(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                this.panel2.ClientRectangle,
                Color.MediumOrchid,
                2,
                ButtonBorderStyle.Solid,
                Color.MediumOrchid,
                2,
                ButtonBorderStyle.Solid,
                Color.MediumOrchid,
                2,
                ButtonBorderStyle.Solid,
                Color.MediumOrchid,
                2,
                ButtonBorderStyle.Solid
                );
        }
        #endregion

        /////////////////////

        private void panel2_SizeChanged_1(object sender, EventArgs e)
        {
            panel2.Refresh();
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("S7-ST30SMART监控操作系统    V1.0.0" +
                System.Environment.NewLine + "版权所有(C)2022-2043" +
                System.Environment.NewLine + "保留所有权利" + "开发者  SunPro" +
                System.Environment.NewLine + "邮箱:2512007208@qq.com",
                "关于", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {


            System.Diagnostics.Process.Start("C:\\Users\\Sun\\OneDrive\\桌面\\C#上位机_SMART\\WindowsFormsApp1\\image\\s7-200_SMART_system_manual_zh-CHS.pdf");
        }
    }
}
