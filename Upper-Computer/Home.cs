﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using Timer = System.Windows.Forms.Timer;

namespace WindowsFormsApp1
{
    public partial class Home : Form
    {
        
        

        用户管理密码 frm_userPass = new 用户管理密码();
        用户管理 frm = new 用户管理();
        public static Timer Clock = new Timer();
        

        public bool isLogin = false;//登录标志

        
        public Home()
        {
            InitializeComponent();
            Clock.Interval = 10;
            Clock.Tick += timer1_Tick;
            
            

        }        

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Color FColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(251)))));
            Color TColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(234)))));

            Brush b = new LinearGradientBrush(this.ClientRectangle, TColor, FColor, LinearGradientMode.ForwardDiagonal);
            g.FillRectangle(b, this.ClientRectangle);

            
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Color FColor = Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(198)))), ((int)(((byte)(239)))));
            Color TColor = Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(134)))), ((int)(((byte)(214)))));

            Brush b = new LinearGradientBrush(this.ClientRectangle, FColor, TColor, LinearGradientMode.ForwardDiagonal);
            g.FillRectangle(b, this.ClientRectangle);
        }

        private void Home_Load(object sender, EventArgs e)
        {
            登录界面 frm = new 登录界面();

            frm.mainHome = this;
            frm.ShowDialog();
            if (isLogin)
            {
                //MessageBox.Show("成功");可以不显示
            }
            else
            {
                this.Close();
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (frm ==  null ||frm.IsDisposed)
            {

                Timer Clock = new Timer();
                //用户管理 frm = new 用户管理();               
            }
            
            frm_userPass.MdiParent = this;
            frm_userPass.Parent = this.splitContainer1.Panel2;
            frm_userPass.TopLevel = false;

            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(frm_userPass);
            frm_userPass.Show();

            Clock.Enabled = false;
            //timer1.Enabled = true;
            
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (frm_userPass.textBox1.Text == "123")
            {
                this.splitContainer1.Panel2.Controls.Clear();

                frm.TopLevel = false;
                frm.Parent = this.splitContainer1.Panel2;
                this.splitContainer1.Panel2.Controls.Add(frm);
                frm.Show();
                //timer1.Enabled = false;
                Clock.Enabled = false;
            }
            else
            {
                timer1.Enabled = false;
                Clock.Enabled = false;
                MessageBox.Show("密码错误!");
               
                
            }


        }

        private void button10_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);
            手动操作 frm = new 手动操作();
            frm.MdiParent = this;
            frm.Parent = this.splitContainer1.Panel2;
            frm.TopLevel = false;

            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(frm);
            frm.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);
            参数设置 frm = new 参数设置();
            frm.MdiParent = this;
            frm.Parent = this.splitContainer1.Panel2;
            frm.TopLevel = false;

            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(frm);
            frm.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);

            监控画面 frm = new 监控画面();

            frm.MdiParent = this;
            frm.Parent = this.splitContainer1.Panel2;
            frm.TopLevel = false;

            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(frm);
            frm.Show();

        }

        private void button13_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);

            报警信息 frm = new 报警信息();
            frm.MdiParent = this;
            frm.Parent = this.splitContainer1.Panel2;
            frm.TopLevel = false;

            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(frm);
            frm.Show();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            //System.Console.Beep(3000, 200);
            数据库画面 frm = new 数据库画面();

            frm.MdiParent = this;
            frm.Parent = this.splitContainer1.Panel2;
            
            frm.TopLevel = false;

            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(frm);
            frm.Show();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.splitContainer1.Panel2.Controls.Clear();
        }
        

        private void 退出XToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button9_MouseEnter(object sender, EventArgs e)
        {
            
        }
    }

            
      
       

          
    
}
